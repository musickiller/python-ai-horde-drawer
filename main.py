import PIL
import io

from horde_model_reference.model_reference_manager import ModelReferenceManager
from horde_sdk import ANON_API_KEY
from horde_sdk.ai_horde_api import KNOWN_SAMPLERS
from horde_sdk.ai_horde_api.ai_horde_clients import AIHordeAPISimpleClient
from horde_sdk.ai_horde_api.apimodels import (
    ImageGenerateAsyncRequest,
    ImageGenerationInputPayload,
    LorasPayloadEntry,
)
from horde_sdk.ai_horde_worker.model_meta import ImageModelLoadResolver

from telegram import Update
from telegram.ext import (
    filters,
    MessageHandler,
    ApplicationBuilder,
    CommandHandler,
    ContextTypes,
    PicklePersistence,
)

import logging

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.WARNING
)


async def start(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text="This is yet another Stable Diffusion drawer bot. It uses AI Horde to generate images. Please see https://aihorde.net/ for more information.",
    )


async def help(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text="""This is yet another Stable Diffusion drawer bot. It uses AI Horde to generate images.
Please see https://aihorde.net/ for more information.

Commands:
/start
/help
/token [token] - get/set token;
/model [model] - get/set model;
/n <number> - set number of generated images
/searchModels <model>
/list_models - list supported models""",
    )


MODELS = ImageModelLoadResolver(ModelReferenceManager()).resolve_all_model_names()


async def model(update: Update, context: ContextTypes.DEFAULT_TYPE):
    if len(context.args) == 0:
        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text="Selected model(s): " + "; ".join(context.user_data.get("models", [])),
        )
        return
    parsed_models_list: list = reparse_models(context.args)
    models: list = [x for x in parsed_models_list if x in MODELS]
    if len(models) == 0:
        await context.bot.send_message(
            chat_id=update.effective_chat.id, text="Invalid model name(s)"
        )
        return
    context.user_data["models"] = models
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text="Changed model(s) to: " + ", ".join(models),
    )


async def change_number(update: Update, context: ContextTypes.DEFAULT_TYPE):
    if len(context.args) == 0:
        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text="Number of images: " + str(context.user_data.get("n", 1)),
        )
        return
    n = int(context.args[0])
    context.user_data["n"] = n
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text="Changed number of images to: " + str(n) + " per model",
    )


async def token(update: Update, context: ContextTypes.DEFAULT_TYPE):
    if len(context.args) == 0:
        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=f"Token: {context.user_data.get('token', 'None')}.",
        )
        return
    context.user_data["token"] = context.args[0]
    await context.bot.send_message(chat_id=update.effective_chat.id, text="Token set.")


async def list_models(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_message(
        chat_id=update.effective_chat.id, text="Supported models: " + ", ".join(MODELS)
    )


async def searchModels(update: Update, context: ContextTypes.DEFAULT_TYPE):
    filtered_models = [x for x in MODELS if x.find(" ".join(context.args)) != -1]
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=(
            "; ".join(filtered_models)
            if len(filtered_models) > 0
            else "No models found with that query."
        ),
    )


async def text(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=f'Generating image with prompt: "{update.message.text}". Please wait.',
    )
    for image in request_image(update.message.text, context):
        await context.bot.send_photo(chat_id=update.effective_chat.id, photo=image)


# this one should be last
async def unknown(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text="Sorry, I didn't understand that command.",
    )


def reparse_models(args: list[str]) -> list[str]:
    original_args = " ".join(args)
    return [x for x in original_args.split(";") if x in MODELS]


def request_image(prompt: str, context) -> list[bytes]:
    models = context.user_data.get("models", ["Deliberate"])

    image_generate_async_request = ImageGenerateAsyncRequest(
        apikey=context.user_data.get("token", ANON_API_KEY),
        prompt=prompt,
        models=models,
        params=ImageGenerationInputPayload(
            width=512,
            height=512,
            sampler_name=KNOWN_SAMPLERS.k_euler_a,
            clip_skip=1,
            n=context.user_data.get("n", 1) * len(models),
        ),
    )

    simple_client = AIHordeAPISimpleClient()
    status_response, job_id = simple_client.image_generate_request(
        image_generate_async_request
    )
    images = []
    print("number of generations:" + str(len(status_response.generations)))
    for gen in status_response.generations:
        image: PIL.Image.Image = simple_client.download_image_from_generation(gen)
        imgByteArr = io.BytesIO()
        image.save(imgByteArr, "JPEG")
        images.append(imgByteArr.getvalue())
    return images


if __name__ == "__main__":
    bot_token = "no_token"
    with open("token_bot.txt") as f:
        bot_token = f.read()
    my_persistence = PicklePersistence(filepath="persistence_file_AiHorde")
    application = (
        ApplicationBuilder()
        .token(bot_token)
        .persistence(persistence=my_persistence)
        .build()
    )

    application.add_handler(CommandHandler("start", start))
    application.add_handler(CommandHandler("help", help))
    application.add_handler(CommandHandler("listModels", list_models))
    application.add_handler(CommandHandler("model", model))
    application.add_handler(CommandHandler("n", change_number))
    application.add_handler(CommandHandler("searchModels", searchModels))
    application.add_handler(CommandHandler("token", token))
    application.add_handler(
        MessageHandler(filters.TEXT & (~filters.COMMAND), text, block=False)
    )

    application.run_polling()
